<?php
class DatabaseGtw
{
    public $error;
    private $lastInserted;

    public function __construct(){

    }

    private function connect(){
        $user = "root";
        $pwd = "root";
        try{
            return new PDO('mysql:host=localhost:3307;dbname=test', $user, $pwd);
        }
        catch(Exception $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    public function query($sql,$datas = null){

        $dbh = $this->connect();
        $stmt = $dbh->prepare($sql);

        if(is_array($datas)){
            foreach($datas as $key => $data) {
                $stmt->bindParam($key, $data);
            }
        }

        if($stmt->execute()){
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        }else{
            throw new Exception('Query impossible.');
        }
        return false;
    }

    public function transSQL($sql,$datas){
        $dbh = $this->connect();
        $stmt = $dbh->prepare($sql);
        $stmt->execute($datas);
        $this->lastInserted = $dbh->lastInsertId();
        return $this;
    }

    public function getLastInserted(){
        return $this->query("SELECT * FROM posts WHERE id = :id",[':id' => $this->lastInserted]);

    }

}